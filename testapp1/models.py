from django.db import models
from django.db.models import signals
from django.dispatch import receiver
from django.conf import settings

import sys
from imp import reload
from django.core.urlresolvers import get_resolver

from autoslug import AutoSlugField
from djrichtextfield.models import (
        RichTextField,
)
from ckeditor_uploader.fields import RichTextUploadingField
from any_imagefield.models import AnyImageField

from snreloader.update_serial import update_serial

def pages_images():
    return ''

def reload_urlconf(urlconf=None):
    if urlconf is None:
        urlconf = settings.ROOT_URLCONF
    if urlconf in sys.modules:
        reload(sys.modules[urlconf])
    #delattr(get_resolver(None), '_urlconf_module')
    #delattr(get_resolver(None), 'urlconf_module')
    reload(getattr(get_resolver(None), 'urlconf_module'))

class Page(models.Model):
    is_published = models.BooleanField(default=False,
                                       help_text='Only items set as published will be shown on the site.')
    show_in_top_menu = models.BooleanField(default=True)
    # is_hidden = models.BooleanField(default=False,
    #                                 help_text='Items can be published and hidden from navigation at the same time.')
    parent = models.ForeignKey('self',
                               related_name='children',
                               blank=True,
                               null=True,
                               on_delete=models.SET_NULL)
    title = models.CharField(max_length=50)
    slug = AutoSlugField(populate_from='title',
                         always_update=True,
                         unique=True)
    clean_uri = models.CharField(max_length=255, blank=True)
    short_description = RichTextField(blank=True)
    text = RichTextUploadingField(blank=True)
    image = AnyImageField(
        upload_to=pages_images,
        max_length=250,
        blank=True,
        help_text='The bigger image the better. It is going to be cropped and compressed automatically.')

    meta_title_db = models.CharField(max_length=255,
                                     blank=True,
                                     verbose_name='meta title',
                                     help_text='If empty, title will be used')
    meta_description_db = models.CharField(max_length=255,
                                           blank=True,
                                           verbose_name='meta description',
                                           help_text='If empty, truncated page text will be used')
    meta_keywords_db = models.CharField(max_length=255,
                                        blank=True,
                                        verbose_name='meta keywords',
                                        help_text='Comma separated list of keywords.')
    order = models.PositiveIntegerField(default=0,
                                        null=False,
                                        db_index=True)
    def get_absolute_url(self):
        return "/page/%s/" % self.slug





@receiver(signals.post_save, sender=Page)
def update_clean_uri_signal(sender, instance, created, **kwargs):
    signals.post_save.disconnect(update_clean_uri_signal, sender=sender)
    instance.clean_uri = instance.get_absolute_url().strip('/')
    instance.save()
    #reload_urlconf()
    update_serial()
    signals.post_save.connect(update_clean_uri_signal, sender=sender)
