from django.db import models

from testapp1.models import Page

class WebsiteSettings(models.Model):
    updates_page = models.ForeignKey(Page,
                               blank=True,
                               null=True)
