from django.apps import AppConfig


class WebsiteContentConfig(AppConfig):
    name = 'website_content'
