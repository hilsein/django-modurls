from django.shortcuts import render
from django.http import HttpResponse


def updates(request):
    return HttpResponse("Updates")

def news(request):
    return HttpResponse("News")

def news_item(request, slug):
    return HttpResponse("News Item: {}".format(slug))
