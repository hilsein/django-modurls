from django.conf.urls import url

from .models import WebsiteSettings
from . import views
from snreloader.load_serial import load_serial

load_serial()

PERIODICALS_ROOT = WebsiteSettings.objects.get().updates_page.clean_uri

urlpatterns = [
    url(r'^{0}/$'.format(PERIODICALS_ROOT), views.updates, name='updates'),
    url(r'^{0}/news/$'.format(PERIODICALS_ROOT), views.news, name='news'),
    url(r'^{0}/news/(?P<slug>[-_\w]+)/$'.format(PERIODICALS_ROOT), views.news_item, name='news_item'),
]
